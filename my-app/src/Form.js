import {useState} from 'react';
import Dropdown from './Dropdown';
import Checkbox from './Checkbox';
import Input from './Input';

export default function Form() {
    return (
      <form>
        <Input />
        <Dropdown />
        <Checkbox />
      </form>
    )
}